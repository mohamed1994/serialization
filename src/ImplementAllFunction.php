<?php


namespace App;


use App\Entity\Categories;
use App\Entity\Criteres;
use App\Entity\Modalites;
use App\Entity\Periode;
use App\Repository\SerializeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ImplementAllFunction extends AbstractController implements AllFunctionServiceInterface
{
    private $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return Serializer
     */
    public function serializerFunction()
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new GetSetMethodNormalizer(), new ArrayDenormalizer(), new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        return $serializer;
    }
    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeListCategories($tab)
    {
        $categories = [];
        $serializer = $this->serializerFunction();
        $categorie = $serializer->decode($tab, 'xml');
        foreach ($categorie as $row) {
            foreach ($row as $row2) {
                $categories[] = $serializer->denormalize(['id' => $row2['@id'], 'gestionDispo' => $row2['@gestionDispo'], 'nbFils' => $row2['@nbFils'], 'niveau' => $row2['@niveau'], 'categorie' => $row2['#']], 'App\Entity\Categorie', 'xml',
                    ['categorie' => ['gestionDispo' => '', 'id' => '', 'nbFils' => '', 'niveau' => '', 'pere' => '', 'categorie' => '']]);
            }
        }
        return $categories;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeListCritere($tab)
    {
        $criteres = [];
        $serializer = $this->serializerFunction();
        if ($tab['criteresSpecs'] == "") {
            $criteres[] = $serializer->denormalize(['id' => '', 'idsToutesCategories' => '',
                'princ' => '', 'type' => '', 'intCritere' => '', 'modalites' => '',
            ],
                'App\Entity\Critere', 'xml');
        } else {
            foreach ($tab['criteresSpecs']['critere'] as $row) {
                $criteres[] = $serializer->denormalize(['id' => $row['@id'], 'idsToutesCategories' => $row['@idsToutesCategories'],
                    'princ' => $row['@princ'], 'type' => $row['@type'], 'intCritere' => $row['intCritere'], 'modalites' => $this->denormalizeModalities($row['modalites']['modalite']),
                ],
                    'App\Entity\Critere', 'xml');
            }
        }
        return $criteres;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeModalities($tab)
    {
        $modalities = [];
        $serializer = $this->serializerFunction();
        if (count($tab, COUNT_RECURSIVE) == 3) {
            $modalities[] = $serializer->denormalize(['id' => $tab['@id'], 'num' => $tab['@num'],
                'modalite' => $tab['#']
            ],
                'App\Entity\Modalite', 'xml');
        } else {
            foreach ($tab as $item) {
                $modalities[] = $serializer->denormalize(['id' => $item['@id'], 'num' => $item['@num'],
                    'modalite' => $item['#']
                ],
                    'App\Entity\Modalite', 'xml');
            }
        }
        return $modalities;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeCategories($tab)
    {
        $serializer = $this->serializerFunction();
        $categorie[] = $serializer->denormalize(['categorie' => $tab['intCategorie'], 'pictoCategorie' => $tab['pictoCategorie']],
            'App\Entity\Categorie', 'xml');
        return $categorie;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeVille($tab)
    {
        $serializer = $this->serializerFunction();
        $ville[] = $serializer->denormalize(['intituleVille' => $tab['intituleVille'], 'latitude' => $tab['@latitude'], 'longitude' => $tab['@longitude'],
            'cpVille' => $tab['cpVille'], 'inseeVille' => $tab['inseeVille']],
            'App\Entity\Ville', 'xml');
        return $ville;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizePays($tab)
    {
        $serializer = $this->serializerFunction();
        $pays[] = $serializer->denormalize(['intPays' => $tab['intPays'], 'logoPays' => $tab['logoPays']],
            'App\Entity\Pays', 'xml');
        return $pays;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizePartenaire($tab)
    {
        $serializer = $this->serializerFunction();
        $partenaire[] = $serializer->denormalize(['intPartenaire' => $tab['intPartenaire'], 'logoPartenaire' => $tab['logoPartenaire'],
            'emailPartenaire' => $tab['emailPartenaire'], 'webPartenaire' => $tab['webPartenaire']],
            'App\Entity\Partenaire', 'xml');
        return $partenaire;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeDatevalidite($tab)
    {
        $serializer = $this->serializerFunction();
        $dateValidite[] = $serializer->denormalize(['dateValiditeDebut' => $tab['dateValiditeDebut'], 'dateValiditeFin' => $tab['dateValiditeFin']],
            'App\Entity\Datevalidite', 'xml');
        return $dateValidite;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeCommentaire($tab)
    {
        $serializer = $this->serializerFunction();
        $commentaires[] = $serializer->denormalize(['commentaire' => $tab['commentaire1']],
            'App\Entity\Commentaire', 'xml');
        return $commentaires;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeAdresses($tab)
    {
        $adresses = [];
        $serializer = $this->serializerFunction();
        if (count($tab, COUNT_RECURSIVE) <= 30) {
            $adresses[] = $serializer->denormalize(['raisonSociale' => $tab['adresse']['raisonSociale'], 'prenom' => $tab['adresse']['prenom'],
                'nom' => $tab['adresse']['nom'], 'fonction' => $tab['adresse']['fonction'], 'ligne1' => $tab['adresse']['ligne1'],
                'ligne2' => $tab['adresse']['ligne2'], 'ligne3' => $tab['adresse']['ligne3'], 'cp' => $tab['adresse']['cp'],
                'commune' => $this->denormalizeCommune($tab['adresse']['commune']), 'paysMonde' => $this->denormalizePaysMonde($tab['adresse']['paysMonde']), 'tel1' => $tab['adresse']['tel1'], 'tel2' => $tab['adresse']['tel2'],
                'fax' => $tab['adresse']['fax'], 'email' => $tab['adresse']['email'], 'web' => $tab['adresse']['web'], 'facebook' => $tab['adresse']['facebook'],
                'twitter' => $tab['adresse']['twitter'], 'googlePlus' => $tab['adresse']['googlePlus'], 'tumblr' => $tab['adresse']['tumblr'],
                'linkedIn' => $tab['adresse']['linkedIn'], 'viadeo' => $tab['adresse']['viadeo'], 'instagram' => $tab['adresse']['instagram']],
                'App\Entity\Adresse', 'xml');
        } else {
            foreach ($tab['adresse'] as $item) {
                $adresses[] = $serializer->denormalize(['raisonSociale' => $item['raisonSociale'], 'prenom' => $item['prenom'],
                    'nom' => $item['nom'], 'fonction' => $item['fonction'], 'ligne1' => $item['ligne1'],
                    'ligne2' => $item['ligne2'], 'ligne3' => $item['ligne3'], 'cp' => $item['cp'],
                    'commune' => $this->denormalizeCommune($item['commune']), 'paysMonde' => $this->denormalizePaysMonde($item['paysMonde']), 'tel1' => $item['tel1'], 'tel2' => $item['tel2'],
                    'fax' => $item['fax'], 'email' => $item['email'], 'web' => $item['web'], 'facebook' => $item['facebook'],
                    'twitter' => $item['twitter'], 'googlePlus' => $item['googlePlus'], 'tumblr' => $item['tumblr'],
                    'linkedIn' => $item['linkedIn'], 'viadeo' => $item['viadeo'], 'instagram' => $item['instagram']],
                    'App\Entity\Adresse', 'xml');
            }
        }
        return $adresses;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeCommune($tab)
    {
        $serializer = $this->serializerFunction();
        $photo[] = $serializer->denormalize(['commune' => $tab['#']],
            'App\Entity\Commune', 'xml');
        return $photo;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizePaysMonde($tab)
    {
        $serializer = $this->serializerFunction();
        $paysmonde[] = $serializer->denormalize(['pays' => $tab['#'], 'iso' => $tab['@iso']],
            'App\Entity\PaysMonde', 'xml');
        return $paysmonde;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeCritere($tab)
    {
        $criteres = [];
        $serializer = $this->serializerFunction();
        if (count($tab, COUNT_RECURSIVE) <= 73) {
            foreach ($tab as $item) {
                if ($item['modalites'] == "") {
                    $criteres[] = $serializer->denormalize(['intCritere' => $item['intCritere']],
                        'App\Entity\Critere', 'xml');
                } else {
                    $criteres[] = $serializer->denormalize(['intCritere' => $item['intCritere'], 'modalites' => $this->denormalizeModalitiesCritere($item['modalites']['modalite'])],
                        'App\Entity\Critere', 'xml');
                }
            }
        } else {
            foreach ($tab as $item) {
                if($item['modalites'] != ""){
                    $criteres[] = $serializer->denormalize(['intCritere' => $item['intCritere'], 'modalites' => $this->denormalizeModalitiesCritere($item['modalites']['modalite'])],
                        'App\Entity\Critere', 'xml');
                }
            }
        }
        return $criteres;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeModalitiesCritere($tab)
    {
        $modalites = [];
        $serializer = $this->serializerFunction();
        if (count($tab, COUNT_RECURSIVE) < 6) {
            $modalites[] = $serializer->denormalize(['intModalite' => $tab['intModalite'], 'valModalite' => $tab['valModalite']],
                'App\Entity\Modalite', 'xml');
        } else {
            foreach ($tab as $row) {
                $modalites[] = $serializer->denormalize(['intModalite' => $row['intModalite'], 'valModalite' => $row['valModalite']],
                    'App\Entity\Modalite', 'xml');
            }
        }
        return $modalites;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeDispo($tab)
    {
        $dispos = [];
        $serializer = $this->serializerFunction();
        foreach ($tab['dispo'] as $item) {
            $dispos[] = $serializer->denormalize(['annee' => $item['@annee']], 'App\Entity\Dispo', 'xml');
        }
        return $dispos;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeNewPhoto($tab)
    {
        $newphoto = [];
        $serializer = $this->serializerFunction();
        if (1 < count($tab, COUNT_RECURSIVE) && count($tab, COUNT_RECURSIVE) <= 7) {
            $newphoto[] = $serializer->denormalize(['photo' => $tab['newPhoto']['#']], 'App\Entity\Newphoto', 'xml');
        } elseif ($tab == "") {
            $newphoto = [];
        } else {
            foreach ($tab['newPhoto'] as $item) {
                $newphoto[] = $serializer->denormalize(['photo' => $item['#']], 'App\Entity\Newphoto', 'xml');
            }
        }
        return $newphoto;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeDetailProduct($tab)
    {
        $details = [];
        $serializer = $this->serializerFunction();
        $resultats = $serializer->decode(utf8_decode($tab), 'xml');
        if (array_key_exists('pays', $resultats)) {
            $details[] = $serializer->denormalize([
                'produit' => $resultats['intitule'],
                'categorie' => $this->denormalizeCategories($resultats['categorie']),
                'ville' => $this->denormalizeVille($resultats['ville']),
                'pays' => $this->denormalizePays($resultats['pays']),
                'partenaires' => $this->denormalizePartenaire($resultats['partenaire']),
                'dateValidite' => $this->denormalizeDatevalidite($resultats['dateValidite']),
                'commentaires' => $this->denormalizeCommentaire($resultats['commentaires']),
                'adresses' => $this->denormalizeAdresses($resultats['adresses']),
                'criteres' => $this->denormalizeCritere($resultats['criteres']['critere']),
                'dispos' => $this->denormalizeDispo($resultats['dispos']),
                'newPhotos' => $this->denormalizeNewPhoto($resultats['newPhotos']),
                'intituleGb' => $resultats['intituleGb'],
                'intituleDe' => $resultats['intituleDe'],
                'intituleNl' => $resultats['intituleNl'],
                'intituleEs' => $resultats['intituleEs'],
                'ouvertureAnnee' => $resultats['ouvertureAnnee'],
                'horairesOuvertures' => $resultats['horairesOuvertures'],
                'periodesOuvertures' => $resultats['periodesOuvertures'],
                'periodesOuverturesOld' => $resultats['periodesOuverturesOld'],
                'dateMAJ' => $resultats['dateMAJ'],
                'dateCreation' => $resultats['dateCreation'],
                'dateMajDispoPart' => $resultats['dateMajDispoPart'],
                'dateMajDispoPrest' => $resultats['dateMajDispoPrest'],
                'dateMajContenuPrest' => $resultats['dateMajContenuPrest'],
                'resumeSlogan' => $resultats['resumeSlogan'],
                'liensMultimedia' => $resultats['liensMultimedia'],
                'fichiersJoints' => $resultats['fichiersJoints'],
                'nbConsultations' => $resultats['@nbConsultations'],
                'nbAvis' => $resultats['@nbAvis'],
                'reference' => $resultats['@reference']
            ],
                'App\Entity\Produit', 'xml');
        } else {
            $details[] = $serializer->denormalize([
                'produit' => $resultats['intitule'],
                'categorie' => $this->denormalizeCategories($resultats['categorie']),
                'ville' => $this->denormalizeVille($resultats['ville']),
                'partenaires' => $this->denormalizePartenaire($resultats['partenaire']),
                'dateValidite' => $this->denormalizeDatevalidite($resultats['dateValidite']),
                'commentaires' => $this->denormalizeCommentaire($resultats['commentaires']),
                'adresses' => $this->denormalizeAdresses($resultats['adresses']),
                'criteres' => $this->denormalizeCritere($resultats['criteres']['critere']),
                'dispos' => $this->denormalizeDispo($resultats['dispos']),
                'newPhotos' => $this->denormalizeNewPhoto($resultats['newPhotos']),
                'intituleGb' => $resultats['intituleGb'],
                'intituleDe' => $resultats['intituleDe'],
                'intituleNl' => $resultats['intituleNl'],
                'intituleEs' => $resultats['intituleEs'],
                'ouvertureAnnee' => $resultats['ouvertureAnnee'],
                'horairesOuvertures' => $resultats['horairesOuvertures'],
                'periodesOuvertures' => $resultats['periodesOuvertures'],
                'periodesOuverturesOld' => $resultats['periodesOuverturesOld'],
                'dateMAJ' => $resultats['dateMAJ'],
                'dateCreation' => $resultats['dateCreation'],
                'dateMajDispoPart' => $resultats['dateMajDispoPart'],
                'dateMajDispoPrest' => $resultats['dateMajDispoPrest'],
                'dateMajContenuPrest' => $resultats['dateMajContenuPrest'],
                'resumeSlogan' => $resultats['resumeSlogan'],
                'liensMultimedia' => $resultats['liensMultimedia'],
                'fichiersJoints' => $resultats['fichiersJoints'],
                'nbConsultations' => $resultats['@nbConsultations'],
                'nbAvis' => $resultats['@nbAvis'],
                'reference' => $resultats['@reference']
            ],
                'App\Entity\Produit', 'xml');
        }
        return $details;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeDetailProductMap($tab)
    {
        $serializer = $this->serializerFunction();
        $resultats = $serializer->decode($tab, 'xml');
        $details[] = $serializer->denormalize([
            'produit' => $resultats['intitule'], 'id' => $resultats['@id'],
            'criteres' => $this->denormalizeCritere($resultats['criteres']['critere'])
        ],
            'App\Entity\Produit', 'xml');
        return $details;
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeListProduct($tab)
    {
        $products = [];
        $serializer = $this->serializerFunction();
        if ($tab['@nbTotal'] == 1) {
            $products[] = $serializer->denormalize(['id' => $tab['produit']['@id'], 'idc' => $tab['produit']['@idc'], 'il' => $tab['produit']['@il'], 'nbConsultations' => $tab['produit']['@nbConsultations'],
                'pic' => $tab['produit']['@pic'], 'produit' => $tab['produit']['#']],
                'App\Entity\Produit', 'xml');
        } elseif ($tab['@nbTotal'] == 0) {
            $products[] = $serializer->denormalize(['id' => '', 'idc' => '', 'il' => '', 'nbConsultations' => '',
                'pic' => '', 'produit' => ''], 'App\Entity\Produit', 'xml');
        } else {
            foreach ($tab['produit'] as $row) {
                $products[] = $serializer->denormalize(['id' => $row['@id'], 'idc' => $row['@idc'], 'il' => $row['@il'], 'nbConsultations' => $row['@nbConsultations'],
                    'pic' => $row['@pic'], 'produit' => $row['#']],
                    'App\Entity\Produit', 'xml');
            }
        }
        return $products;
    }

    /**
     * @param $file
     * @return string|string[]|null
     */
    public function domData($file)
    {
        $dom = new \DOMDocument();
        $flux = utf8_encode(file_get_contents($file));
        $dom->loadXML($flux);
        set_time_limit(1000);
        return preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', $dom->saveXML());
    }

    /**
     * @param $tab
     * @return object
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeContact($tab)
    {
        $serializer = $this->serializerFunction();
            return $serializer->denormalize(['idContact' => 1,
                    'siteweb' => $this->stringVerification($tab['d:MoyenComSiteWeb']),
                    'adresse1' => $this->stringVerification($tab['d:Adresse1']),
                    'adresse2' => $this->stringVerification($tab['d:Adresse1Suite']),
                    'email' => $this->stringVerification($tab['d:MoyenComMail']),
                    'telephone' => $this->stringVerification($tab['d:MoyenComTelephone'])]
                , 'App\Entity\Contact', 'xml');
    }

    /**
     * @param $tab
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizePhoto($tab)
    {
        $photo = [];
        $i = 1;
        $serializer = $this->serializerFunction();
        foreach ($tab as $item) {
            $photo[] = $serializer->denormalize(['idPhoto' => $i, 'photo' => $item]
                , 'App\Entity\Photo', 'xml');
            $i++;
        }
        return $photo;
    }

    /**
     * @param $tab
     * @param $author
     * @return mixed
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeProduct($tab,$author){
        $serializer = $this->serializerFunction();
            return $serializer->denormalize(['title' => $this->stringVerification($tab['title']),'nom' =>$this->stringVerification($tab['content']['m:properties']['d:Nom']),'SyndicObjectID' => $this->stringVerification($tab['content']['m:properties']['d:SyndicObjectID']),
                    'Published' => $this->stringVerification($tab['content']['m:properties']['d:Published']),'Updated' => $this->stringVerification($tab['content']['m:properties']['d:Updated']), 'SyndicObjectName' => $this->stringVerification($tab['content']['m:properties']['d:SyndicObjectName']),
                    'SyndicStructureId' => $this->stringVerification($tab['content']['m:properties']['d:SyndicStructureId']),'ObjectTypeFix' => $this->stringVerification($tab['content']['m:properties']['d:ObjectTypeFix']), 'ObjectTypeName' => $this->stringVerification($tab['content']['m:properties']['d:ObjectTypeName']),
                    'SyndicObjectOrder' => $this->stringVerification($tab['content']['m:properties']['d:SyndicObjectOrder']),'tourismehandicap' => $this->stringVerification($tab['content']['m:properties']['d:LabelTourismeHandicap']),
                    'descriptifcommercialgeneral' => $this->stringVerification($tab['content']['m:properties']['d:DescriptionCommericale']),
                    'widgettripadvisor' => $this->stringVerification($tab['content']['m:properties']['d:Tripadvisor']),
                    'author' => $author ]
                , 'App\Entity\Product', 'xml');
    }

    /**
     * @param $tab
     * @return object
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalizeVilles($tab)
    {
        $serializer = $this->serializerFunction();
        return $serializer->denormalize(['codepostal' => $tab['d:CodePostal'],
                'latitude' => $tab['d:GmapLatitude'], 'longitude' => $tab['d:GmapLongitude']]
            , 'App\Entity\Villes', 'xml');
    }

    /**
     * @param $src
     * @return Response
     * @throws NonUniqueResultException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function deserializeAndSendDataToDataBase($src)
    {
        $data = $this->domData($src);
        $serializer = $this->serializerFunction();
        $entityManager = $this->getDoctrine()->getManager();
        $result = $serializer->decode(utf8_decode($data), 'xml');
        $categorie = new Categories();
        $categorie->setIntCategorie('Hôtellerie');
        foreach ($result['entry'] as $items){
            $j=0;
            $modal = [];
            $criter = [];
            $author = $items['author']['name'];
            $product = $this->denormalizeProduct($items,$author);
            $contact = $this->denormalizeContact($items['content']['m:properties']);
            $ville = $this->denormalizeVilles($items['content']['m:properties']);
            $date = [$this->stringVerification($items['content']['m:properties']['d:OuvertureAccueil'])];
            $tarifs = $this->DelimitedCharacter($items['content']['m:properties']['d:Tarifs']);
            $tarifsn1 = $this->DelimitedCharacter($items['content']['m:properties']['d:TarifsPageListing']);
            $languesparlees = $this->DelimitedCharacter($this->stringVerification($items['content']['m:properties']['d:LanguesParlees']));
            $confort = $this->DelimitedCharacter($this->stringVerification($items['content']['m:properties']['d:ConfortHOTHLO']));
            $commune = [$this->stringVerification($items['content']['m:properties']['d:Commune'])];
            $animaux = [$this->stringVerification($items['content']['m:properties']['d:AnimauxAcceptes'])];
            $nbrechambres = [$this->stringVerification($items['content']['m:properties']['d:NbChambresHLOHOTINS'])];
            $nbchaletshpa = [$this->stringVerification($items['content']['m:properties']['d:NbChaletsHPA'])];
            $nbbungalowshpa = [$this->stringVerification($items['content']['m:properties']['d:NbBungalowsHPA'])];
            $modepaiement = [$this->stringVerification($items['content']['m:properties']['d:ModePaiement'])];
            $thematique = $this->DelimitedCharacter($this->stringVerification($items['content']['m:properties']['d:Themes']));
            $accueilvelo = [$this->stringVerification($items['content']['m:properties']['d:AccueilVelo'])];
            $typeequipement = [$this->stringVerification($items['content']['m:properties']['d:TypeEquipement'])];
            $equipements = [$this->stringVerification($items['content']['m:properties']['d:Equipements'])];
            $geolocalisation = [$items['content']['m:properties']['d:GmapLatitude'],$items['content']['m:properties']['d:GmapLongitude']];
            $resa = [$this->stringVerification($items['content']['m:properties']['d:MComResaMail']),$this->stringVerification($items['content']['m:properties']['d:MComResaPortable']),$this->stringVerification($items['content']['m:properties']['d:MComResaTelephone'])];
            $services = [$this->stringVerification($items['content']['m:properties']['d:Services'])];
            $reseauxsociaux = $this->DelimitedCharacter($this->stringVerification($items['content']['m:properties']['d:ReseauxSociaux']));
            $taxesejour = [$this->stringVerification($items['content']['m:properties']['d:TaxeSejour'])];
            $capacitehebergement = [$this->stringVerification($items['content']['m:properties']['d:CapaciteHebergement'])];
            $qualite_tourisme = [$this->stringVerification($items['content']['m:properties']['d:QualiteTourisme'])];
            $descriptif_habitation_hlo = [$this->stringVerification($items['content']['m:properties']['d:DescriptifHabitationHLO'])];
            $referencegite = [$this->stringVerification($items['content']['m:properties']['d:ReferenceGite'])];
            $nbEmplacementTotalHPAACC = [$this->stringVerification($items['content']['m:properties']['d:NbEmplacementTotalHPAACC'])];
            $zones = [$this->stringVerification($items['content']['m:properties']['d:Zones'])];
            $typeEquipementComplementaireHLO = [$this->stringVerification($items['content']['m:properties']['d:TypeEquipementComplementaireHLO'])];
            $categorieNNHOTHPA = [$this->stringVerification($items['content']['m:properties']['d:CategorieNNHOTHPA'])];
            $activites = [$this->stringVerification($items['content']['m:properties']['d:Activites'])];
            $nbsuiteHOT = [$this->stringVerification($items['content']['m:properties']['d:NbSuitesHOT'])];
            $classementLabelHLOPageListing = [$this->stringVerification($items['content']['m:properties']['d:ClassementLabelHLOPageListing'])];
            $fermesCertainsJours = [$this->stringVerification($items['content']['m:properties']['d:FermesCertainsJours'])];
            $typeLabels = [$this->stringVerification($items['content']['m:properties']['d:TypeLabels'])];
            $fichierPDF = [$this->stringVerification($items['content']['m:properties']['d:FichierPDF'])];
            $classementLabelHBGINS = [$this->stringVerification($items['content']['m:properties']['d:ClassementLabelHBGINS'])];
            $pictoPtitsdecouvreurs = [$this->stringVerification($items['content']['m:properties']['d:PictoPtitsdecouvreurs'])];
            $TIS_TRACKING_OFFRE = [$this->stringVerification($items['content']['m:properties']['d:TIS_TRACKING_OFFRE'])];
            $TIS_TRACKING_LISTING_OFFRE = [$this->stringVerification($items['content']['m:properties']['d:TIS_TRACKING_LISTING_OFFRE'])];
            $photosPageListing = [$this->stringVerification($items['content']['m:properties']['d:PhotosPageListing'])];
            $modalites = [$services,$tarifs,$tarifsn1,$languesparlees,$confort,$commune,$animaux,$nbrechambres,$nbchaletshpa,
                $nbbungalowshpa,$modepaiement,$equipements,$thematique,$accueilvelo,$typeequipement,
                $geolocalisation,$resa,$reseauxsociaux,$taxesejour,$capacitehebergement,
                $qualite_tourisme,$descriptif_habitation_hlo,$referencegite,$nbEmplacementTotalHPAACC,$zones,$typeEquipementComplementaireHLO,
                $categorieNNHOTHPA,$activites,$nbsuiteHOT,$classementLabelHLOPageListing,$fermesCertainsJours,$typeLabels,$fichierPDF,$classementLabelHBGINS,
                $pictoPtitsdecouvreurs,$TIS_TRACKING_OFFRE,$TIS_TRACKING_LISTING_OFFRE,$photosPageListing];
            $criteres = ['services','tarifs','tarifsn1','langues parlees','confort','commune','animaux','nbre chambres','nb chaletshpa','nb bungalowshpa',
                'mode paiement','equipements','thematique','accueil velo','type equipement','geolocalisation','resa','reseaux sociaux','taxe sejour',
                'capacite hebergement','qualite tourisme','descriptif habitation hlo','referencegite','nbEmplacementTotalHPAACC','zones','typeEquipementComplementaireHLO',
                'categorieNNHOTHPA','activites','nbsuiteHOT','classementLabelHLOPageListing','fermesCertainsJours','typeLabels','fichierPDF','classementLabelHBGINS',
                'pictoPtitsdecouvreurs','TIS_TRACKING_OFFRE','TIS_TRACKING_LISTING_OFFRE','photosPageListing'];
            $photo = $this->denormalizePhoto($this->DelimitedCharacter($this->stringVerification($items['content']['m:properties']['d:PhotosPageDetail'])));
            /*
             * Insertion de photo
             */
            foreach ($photo as $item){
                $item->setProduct($product);
                $entityManager->persist($item);
            }
            /*
             * Insertion de period
             */
            foreach ($date as $item){
                if ($item != ''){
                    $period = new Periode();
                    $period->setPeriode($item);
                    $period->setAnnee($this->findYears($item));
                    $period->setDatedebut($this->findDateDebut($item));
                    $period->setDatefin($this->findDateFin($item));
                    $period->setProduct($product);
                    $entityManager->persist($period);
                }
            }
            /*
             * Insertion de critere et modalite
             */
            foreach ($criteres as $item){
                if($this->searchCritere($item) == 0){
                    $critere = new Criteres();
                    $critere->setIntCritere($item);
                    $criter[] = $critere;
                    $categorie->setCritere($criter);
                    $entityManager->persist($critere);
                    $entityManager->persist($categorie);
                }
                if (is_array($modalites[$j]) && !empty($critere)){
                    foreach ($modalites[$j] as $row){
                        if($row != '' ){
                            $modalite = new Modalites();
                            $modalite->setIntModalite($row);
                            if($this->searchCritere($item) == 0){
                                $modalite->setCritere($critere);
                            }else{
                                $modalite->setCritere($this->findCritere($item));
                            }
                            $modal[] = $modalite;
                            $entityManager->persist($modalite);
                        }
                    }
                }
                $j++;
            }
            $product->setContact($contact);
            $product->setVille($ville);
            $product->setGmapLatitude($items['geo:lat']);
            $product->setGmapLongitude($items['geo:long']);
            $product->setModalite($modal);
            $entityManager->persist($product);
            $entityManager->flush();
        }
        return new Response("Data added successfully in database");
    }

    /**
     * @param $data
     * @return array
     */
    public function delimitedCharacter($data){
        $delimiters = $this->getParameter('delimiter');
        return array_filter(explode($delimiters['diez'],$data));
    }

    /**
     * @param $tab
     * @return mixed
     */
    public function findYears($tab)
    {
        $tab = substr($tab, 0, 10);
        if(preg_match('/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/',$tab)){
            $years = explode('/',$tab);
            return $years[2];
        }else{
            return '';
        }
    }
    /**
     * @param $tab
     * @return mixed
     */
    public function findDateDebut($tab)
    {
        $delimiters = $this->getParameter('delimiter');
        $tab = substr($tab, 0, 22);
        $tab = explode($delimiters['pipe'].$delimiters['pipe'],$tab);
        return $tab[0];
    }
    /**
     * @param $tab
     * @return mixed
     */
    public function findDateFin($tab)
    {
        $delimiters = $this->getParameter('delimiter');
        $tab = substr($tab, 0, 22);
        $tab = explode($delimiters['pipe'].$delimiters['pipe'],$tab);
        return $tab[1];
    }
    public function stringVerification($var){
        if(is_string($var)){
            return $var;
        }else{
            return $var['#'];
        }
    }

    /**
     * @param $data
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function searchCritere($data){
        $repository = new SerializeRepository($this->em);
        return $repository->findCritereBy($data);
    }
    /**
     * @param $data
     * @return object|null
     */
    public function findCritere($data){
        $repository = $this->getDoctrine()->getRepository(Criteres::class);
        return $repository->findOneBy([
            'intCritere' => $data,
        ]);
    }
}
