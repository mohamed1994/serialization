<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 27/03/2019
 * Time: 12:16
 */

namespace App\Entity;


class Ville
{
    private $intituleVille;
    private $latitude;
    private $longitude;
    private $inseeVille;
    private $cpVille;

    /**
     * @return mixed
     */
    public function getIntituleVille()
    {
        return $this->intituleVille;
    }

    /**
     * @param mixed $intituleVille
     */
    public function setIntituleVille($intituleVille): void
    {
        $this->intituleVille = $intituleVille;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getInseeVille()
    {
        return $this->inseeVille;
    }

    /**
     * @param mixed $inseeVille
     */
    public function setInseeVille($inseeVille): void
    {
        $this->inseeVille = $inseeVille;
    }

    /**
     * @return mixed
     */
    public function getCpVille()
    {
        return $this->cpVille;
    }

    /**
     * @param mixed $cpVille
     */
    public function setCpVille($cpVille): void
    {
        $this->cpVille = $cpVille;
    }
}
