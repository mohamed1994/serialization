<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 18/03/2019
 * Time: 11:58
 */

namespace App\Entity;

class Categorie
{
    private $id;
    private $gestionDispo;
    private $nbFils;
    private $niveau;
    private $pere;
    private $categorie;
    private $pictoCategorie;

    /**
     * @return mixed
     */
    public function getPictoCategorie()
    {
        return $this->pictoCategorie;
    }

    /**
     * @param mixed $pictoCategorie
     */
    public function setPictoCategorie($pictoCategorie): void
    {
        $this->pictoCategorie = $pictoCategorie;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie): void
    {
        $this->categorie = $categorie;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getGestionDispo()
    {
        return $this->gestionDispo;
    }

    /**
     * @param mixed $gestionDispo
     */
    public function setGestionDispo($gestionDispo): void
    {
        $this->gestionDispo = $gestionDispo;
    }

    /**
     * @return mixed
     */
    public function getNbFils()
    {
        return $this->nbFils;
    }

    /**
     * @param mixed $nbFils
     */
    public function setNbFils($nbFils): void
    {
        $this->nbFils = $nbFils;
    }

    /**
     * @return mixed
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * @param mixed $niveau
     */
    public function setNiveau($niveau): void
    {
        $this->niveau = $niveau;
    }

    /**
     * @return mixed
     */
    public function getPere()
    {
        return $this->pere;
    }

    /**
     * @param mixed $pere
     */
    public function setPere($pere): void
    {
        $this->pere = $pere;
    }
}
