<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 26/04/2019
 * Time: 08:51
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ville
 *
 * @ORM\Table(name="ville"),
 * })
 *
 * @ORM\Entity
 */

class Villes
{
    /**
     * @var int
     *
     * @ORM\Column(name="idVille", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idVille;
    /**
     * @var string
     *
     * @ORM\Column(name="codepostal", type="string")
     */
    private $codepostal;
    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string")
     */
    private $latitude;
    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string")
     */
    private $longitude;
    /**
     * @var string
     *
     * @ORM\Column(name="codeinsee", type="string")
     */
    private $codeinsee;

    /**
     * @return int
     */
    public function getIdVille(): int
    {
        return $this->idVille;
    }

    /**
     * @param int $idVille
     */
    public function setIdVille(int $idVille): void
    {
        $this->idVille = $idVille;
    }

    /**
     * @return string
     */
    public function getCodepostal(): string
    {
        return $this->codepostal;
    }

    /**
     * @param string $codepostal
     */
    public function setCodepostal(string $codepostal): void
    {
        $this->codepostal = $codepostal;
    }

    /**
     * @return string
     */
    public function getLatitude(): string
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude(string $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getLongitude(): string
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude(string $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getCodeinsee(): string
    {
        return $this->codeinsee;
    }

    /**
     * @param string $codeinsee
     */
    public function setCodeinsee(string $codeinsee): void
    {
        $this->codeinsee = $codeinsee;
    }
}
