<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 24/04/2019
 * Time: 13:07
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Criteres
 *
 * @ORM\Table(name="critere"),
 *
 * })
 * @ORM\Entity
 */

class Criteres
{
    /**
     * @var int
     *
     * @ORM\Column(name="idCritere", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCritere;
    /**
     * @var string
     *
     * @ORM\Column(name="intCritere", type="string")
     */
    private $intCritere;
    /**
     * @return int
     */
    public function getIdCritere(): int
    {
        return $this->idCritere;
    }

    /**
     * @param int $idCritere
     */
    public function setIdCritere(int $idCritere): void
    {
        $this->idCritere = $idCritere;
    }

    /**
     * @return string
     */
    public function getIntCritere(): string
    {
        return $this->intCritere;
    }

    /**
     * @param string $intCritere
     */
    public function setIntCritere(string $intCritere): void
    {
        $this->intCritere = $intCritere;
    }
}
