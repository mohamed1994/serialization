<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 27/03/2019
 * Time: 12:31
 */

namespace App\Entity;


class Datevalidite
{
    private $dateValiditeDebut;
    private $dateValiditeFin;

    /**
     * @return mixed
     */
    public function getDateValiditeDebut()
    {
        return $this->dateValiditeDebut;
    }

    /**
     * @param mixed $dateValiditeDebut
     */
    public function setDateValiditeDebut($dateValiditeDebut): void
    {
        $this->dateValiditeDebut = $dateValiditeDebut;
    }

    /**
     * @return mixed
     */
    public function getDateValiditeFin()
    {
        return $this->dateValiditeFin;
    }

    /**
     * @param mixed $dateValiditeFin
     */
    public function setDateValiditeFin($dateValiditeFin): void
    {
        $this->dateValiditeFin = $dateValiditeFin;
    }
}
