<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 25/04/2019
 * Time: 10:54
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categories
 *
 * @ORM\Table(name="categorie"),
 *
 * })
 * @ORM\Entity
 */
class Categories
{
    /**
     * @var int
     *
     * @ORM\Column(name="idCategorie", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCategorie;
    /**
     * @var string
     *
     * @ORM\Column(name="intCategorie", type="string")
     */
    private $intCategorie;
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Criteres")
     * @ORM\JoinTable(name="categoriecritere",
     *   joinColumns={
     *     @ORM\JoinColumn(name="categorie_id", referencedColumnName="idCategorie")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="critere_id", referencedColumnName="idCritere")
     *   }
     * )
     */
    private $critere;

    public function getCritere()
    {
        return $this->critere;
    }

    public function setCritere($critere)
    {
        $this->critere = $critere;
    }

    /**
     * @return int
     */
    public function getIdCategorie(): int
    {
        return $this->idCategorie;
    }

    /**
     * @param int $idCategorie
     */
    public function setIdCategorie(int $idCategorie): void
    {
        $this->idCategorie = $idCategorie;
    }

    /**
     * @return string
     */
    public function getIntCategorie(): string
    {
        return $this->intCategorie;
    }

    /**
     * @param string $intCategorie
     */
    public function setIntCategorie(string $intCategorie): void
    {
        $this->intCategorie = $intCategorie;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->critere = new \Doctrine\Common\Collections\ArrayCollection();
    }
}
