<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 22/03/2019
 * Time: 09:23
 */

namespace App\Entity;


class Produit
{
    private $id;
    private $idc;
    private $dispo;
    private $il;
    private $nbConsultations;
    private $pic;
    private $produit;
    private $newIntitules;
    private $ville;
    private $pays;
    private $partenaires;
    private $datevalidite;
    private $commentaires;
    private $adresses;
    private $criteres;
    private $dispos;
    private $reference;
    private $categorie;
    private $photo;
    private $newPhotos;
    private $dateMAJ;
    private $dateCreation;
    private $dateMajDispoPart;
    private $dateMajDispoPrest;
    private $dateMajContenuPrest;
    private $newCommentaires;
    private $nbAvis;

    /**
     * @return mixed
     */
    public function getNbAvis()
    {
        return $this->nbAvis;
    }

    /**
     * @param mixed $nbAvis
     */
    public function setNbAvis($nbAvis): void
    {
        $this->nbAvis = $nbAvis;
    }
    /**
     * @return mixed
     */
    public function getDateMAJ()
    {
        return $this->dateMAJ;
    }

    /**
     * @param mixed $dateMAJ
     */
    public function setDateMAJ($dateMAJ): void
    {
        $this->dateMAJ = $dateMAJ;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param mixed $dateCreation
     */
    public function setDateCreation($dateCreation): void
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return mixed
     */
    public function getDateMajDispoPart()
    {
        return $this->dateMajDispoPart;
    }

    /**
     * @param mixed $dateMajDispoPart
     */
    public function setDateMajDispoPart($dateMajDispoPart): void
    {
        $this->dateMajDispoPart = $dateMajDispoPart;
    }

    /**
     * @return mixed
     */
    public function getDateMajDispoPrest()
    {
        return $this->dateMajDispoPrest;
    }

    /**
     * @param mixed $dateMajDispoPrest
     */
    public function setDateMajDispoPrest($dateMajDispoPrest): void
    {
        $this->dateMajDispoPrest = $dateMajDispoPrest;
    }

    /**
     * @return mixed
     */
    public function getDateMajContenuPrest()
    {
        return $this->dateMajContenuPrest;
    }

    /**
     * @param mixed $dateMajContenuPrest
     */
    public function setDateMajContenuPrest($dateMajContenuPrest): void
    {
        $this->dateMajContenuPrest = $dateMajContenuPrest;
    }

    /**
     * @return mixed
     */
    public function getNewCommentaires()
    {
        return $this->newCommentaires;
    }

    /**
     * @param mixed $newCommentaires
     */
    public function setNewCommentaires($newCommentaires): void
    {
        $this->newCommentaires = $newCommentaires;
    }

    /**
     * @return mixed
     */
    public function getNewPhotos()
    {
        return $this->newPhotos;
    }

    /**
     * @param mixed $newPhotos
     */
    public function setNewPhotos($newPhotos): void
    {
        $this->newPhotos = $newPhotos;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie): void
    {
        $this->categorie = $categorie;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     */
    public function setReference($reference): void
    {
        $this->reference = $reference;
    }
    /**
     * @return mixed
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @param mixed $pays
     */
    public function setPays($pays): void
    {
        $this->pays = $pays;
    }

    /**
     * @return mixed
     */
    public function getPartenaires()
    {
        return $this->partenaires;
    }

    /**
     * @param mixed $partenaires
     */
    public function setPartenaires($partenaires): void
    {
        $this->partenaires = $partenaires;
    }

    /**
     * @return mixed
     */
    public function getDatevalidite()
    {
        return $this->datevalidite;
    }

    /**
     * @param mixed $datevalidite
     */
    public function setDatevalidite($datevalidite): void
    {
        $this->datevalidite = $datevalidite;
    }

    /**
     * @return mixed
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param mixed $commentaires
     */
    public function setCommentaires($commentaires): void
    {
        $this->commentaires = $commentaires;
    }
    /**
     * @return mixed
     */
    public function getAdresses()
    {
        return $this->adresses;
    }

    /**
     * @param mixed $adresses
     */
    public function setAdresses($adresses): void
    {
        $this->adresses = $adresses;
    }

    /**
     * @return mixed
     */
    public function getCriteres()
    {
        return $this->criteres;
    }

    /**
     * @param mixed $criteres
     */
    public function setCriteres($criteres): void
    {
        $this->criteres = $criteres;
    }

    /**
     * @return mixed
     */
    public function getDispos()
    {
        return $this->dispos;
    }

    /**
     * @param mixed $dispos
     */
    public function setDispos($dispos): void
    {
        $this->dispos = $dispos;
    }

    /**
     * @return mixed
     */
    public function getNewIntitules()
    {
        return $this->newIntitules;
    }

    /**
     * @param mixed $newIntitules
     */
    public function setNewIntitules($newIntitules): void
    {
        $this->newIntitules = $newIntitules;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     */
    public function setVille($ville): void
    {
        $this->ville = $ville;
    }
    /**
     * @return mixed
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @param mixed $produit
     */
    public function setProduit($produit): void
    {
        $this->produit = $produit;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdc()
    {
        return $this->idc;
    }

    /**
     * @param mixed $idc
     */
    public function setIdc($idc): void
    {
        $this->idc = $idc;
    }

    /**
     * @return mixed
     */
    public function getDispo()
    {
        return $this->dispo;
    }

    /**
     * @param mixed $dispo
     */
    public function setDispo($dispo): void
    {
        $this->dispo = $dispo;
    }

    /**
     * @return mixed
     */
    public function getIl()
    {
        return $this->il;
    }

    /**
     * @param mixed $il
     */
    public function setIl($il): void
    {
        $this->il = $il;
    }

    /**
     * @return mixed
     */
    public function getNbConsultations()
    {
        return $this->nbConsultations;
    }

    /**
     * @param mixed $nbConsultations
     */
    public function setNbConsultations($nbConsultations): void
    {
        $this->nbConsultations = $nbConsultations;
    }

    /**
     * @return mixed
     */
    public function getPic()
    {
        return $this->pic;
    }

    /**
     * @param mixed $pic
     */
    public function setPic($pic): void
    {
        $this->pic = $pic;
    }
}
