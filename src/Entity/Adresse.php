<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 27/03/2019
 * Time: 12:32
 */

namespace App\Entity;


class Adresse
{
    private $id;
    private $type;
    private $civilite;
    private $raisonSociale;
    private $prenom;
    private $nom;
    private $fonction;
    private $ligne1;
    private $ligne2;
    private $ligne3;
    private $cp;
    private $communes;
    private $paysMonde;
    private $tel1;
    private $tel2;
    private $fax;
    private $email;
    private $web;
    private $facebook;
    private $twitter;
    private $googlePlus;
    private $tumblr;
    private $linkedin;
    private $viadeo;
    private $instagram;
    private $commune;

    /**
     * @return mixed
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * @param mixed $linkedin
     */
    public function setLinkedin($linkedin): void
    {
        $this->linkedin = $linkedin;
    }
    /**
     * @return mixed
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * @param mixed $commune
     */
    public function setCommune($commune): void
    {
        $this->commune = $commune;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * @param mixed $civilite
     */
    public function setCivilite($civilite): void
    {
        $this->civilite = $civilite;
    }

    /**
     * @return mixed
     */
    public function getRaisonSociale()
    {
        return $this->raisonSociale;
    }

    /**
     * @param mixed $raisonSociale
     */
    public function setRaisonSociale($raisonSociale): void
    {
        $this->raisonSociale = $raisonSociale;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * @param mixed $fonction
     */
    public function setFonction($fonction): void
    {
        $this->fonction = $fonction;
    }

    /**
     * @return mixed
     */
    public function getLigne1()
    {
        return $this->ligne1;
    }

    /**
     * @param mixed $ligne1
     */
    public function setLigne1($ligne1): void
    {
        $this->ligne1 = $ligne1;
    }

    /**
     * @return mixed
     */
    public function getLigne2()
    {
        return $this->ligne2;
    }

    /**
     * @param mixed $ligne2
     */
    public function setLigne2($ligne2): void
    {
        $this->ligne2 = $ligne2;
    }

    /**
     * @return mixed
     */
    public function getLigne3()
    {
        return $this->ligne3;
    }

    /**
     * @param mixed $ligne3
     */
    public function setLigne3($ligne3): void
    {
        $this->ligne3 = $ligne3;
    }

    /**
     * @return mixed
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * @param mixed $cp
     */
    public function setCp($cp): void
    {
        $this->cp = $cp;
    }

    /**
     * @return mixed
     */
    public function getCommunes()
    {
        return $this->communes;
    }

    /**
     * @param mixed $communes
     */
    public function setCommunes($communes): void
    {
        $this->communes = $communes;
    }

    /**
     * @return mixed
     */
    public function getPaysMonde()
    {
        return $this->paysMonde;
    }

    /**
     * @param mixed $paysMonde
     */
    public function setPaysMonde($paysMonde): void
    {
        $this->paysMonde = $paysMonde;
    }

    /**
     * @return mixed
     */
    public function getTel1()
    {
        return $this->tel1;
    }

    /**
     * @param mixed $tel1
     */
    public function setTel1($tel1): void
    {
        $this->tel1 = $tel1;
    }

    /**
     * @return mixed
     */
    public function getTel2()
    {
        return $this->tel2;
    }

    /**
     * @param mixed $tel2
     */
    public function setTel2($tel2): void
    {
        $this->tel2 = $tel2;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param mixed $fax
     */
    public function setFax($fax): void
    {
        $this->fax = $fax;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * @param mixed $web
     */
    public function setWeb($web): void
    {
        $this->web = $web;
    }

    /**
     * @return mixed
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param mixed $facebook
     */
    public function setFacebook($facebook): void
    {
        $this->facebook = $facebook;
    }

    /**
     * @return mixed
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * @param mixed $twitter
     */
    public function setTwitter($twitter): void
    {
        $this->twitter = $twitter;
    }

    /**
     * @return mixed
     */
    public function getGooglePlus()
    {
        return $this->googlePlus;
    }

    /**
     * @param mixed $googlePlus
     */
    public function setGooglePlus($googlePlus): void
    {
        $this->googlePlus = $googlePlus;
    }

    /**
     * @return mixed
     */
    public function getTumblr()
    {
        return $this->tumblr;
    }

    /**
     * @param mixed $tumblr
     */
    public function setTumblr($tumblr): void
    {
        $this->tumblr = $tumblr;
    }
    /**
     * @return mixed
     */
    public function getViadeo()
    {
        return $this->viadeo;
    }

    /**
     * @param mixed $viadeo
     */
    public function setViadeo($viadeo): void
    {
        $this->viadeo = $viadeo;
    }

    /**
     * @return mixed
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * @param mixed $instagram
     */
    public function setInstagram($instagram): void
    {
        $this->instagram = $instagram;
    }
}
