<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 24/04/2019
 * Time: 17:48
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
/**
 * Modalites
 *
 * @ORM\Table(name="modalite",indexes={@ORM\Index(name="critere", columns={"critere_id"}),
 *
 * })
 * @ORM\Entity
 */
class Modalites
{
    /**
     * @var int
     *
     * @ORM\Column(name="idModalite", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idModalite;
    /**
     * @var string
     *
     * @ORM\Column(name="intModalite", type="string")
     */
    private $intModalite;
    /**
     * @ORM\ManyToOne(targetEntity="Criteres",cascade={"persist"})
     * @ORM\JoinColumn(name="critere_id", referencedColumnName="idCritere")
     */
    private $critere;
    /**
     * @return int
     */
    public function getIdModalite(): int
    {
        return $this->idModalite;
    }

    /**
     * @param int $idModalite
     */
    public function setIdModalite(int $idModalite): void
    {
        $this->idModalite = $idModalite;
    }

    /**
     * @return string
     */
    public function getIntModalite(): string
    {
        return $this->intModalite;
    }

    /**
     * @param string $intModalite
     */
    public function setIntModalite(string $intModalite): void
    {
        $this->intModalite = $intModalite;
    }

    /**
     * @return mixed
     */
    public function getCritere()
    {
        return $this->critere;
    }

    /**
     * @param mixed $critere
     */
    public function setCritere($critere): void
    {
        $this->critere = $critere;
    }
}
