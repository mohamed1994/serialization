<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 26/04/2019
 * Time: 08:46
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Periode
 *
 * @ORM\Table(name="periode",indexes={@ORM\Index(name="product", columns={"product_id"}),
 * })
 *
 * @ORM\Entity
 */
class Periode
{
    /**
     * @var int
     *
     * @ORM\Column(name="idPeriode", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPeriode;
    /**
     * @var string
     *
     * @ORM\Column(name="periode", type="string")
     */
    private $periode;
    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="string")
     */
    private $annee;
    /**
     * @var string
     *
     * @ORM\Column(name="date_debut", type="string")
     */
    private $datedebut;
    /**
     * @var string
     *
     * @ORM\Column(name="date_fin", type="string")
     */
    private $datefin;
    /**
     * @ORM\ManyToOne(targetEntity="Product",cascade={"persist"},fetch="EAGER")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="idProduct")
     */
    private $product;

    /**
     * @return int
     */
    public function getIdPeriode(): int
    {
        return $this->idPeriode;
    }

    /**
     * @param int $idPeriode
     */
    public function setIdPeriode(int $idPeriode): void
    {
        $this->idPeriode = $idPeriode;
    }

    /**
     * @return string
     */
    public function getPeriode(): string
    {
        return $this->periode;
    }

    /**
     * @param string $periode
     */
    public function setPeriode(string $periode): void
    {
        $this->periode = $periode;
    }

    /**
     * @return string
     */
    public function getAnnee(): string
    {
        return $this->annee;
    }

    /**
     * @param string $annee
     */
    public function setAnnee(string $annee): void
    {
        $this->annee = $annee;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }
    /**
     * @return string
     */
    public function getDatedebut(): string
    {
        return $this->datedebut;
    }

    /**
     * @param string $datedebut
     */
    public function setDatedebut(string $datedebut): void
    {
        $this->datedebut = $datedebut;
    }

    /**
     * @return string
     */
    public function getDatefin(): string
    {
        return $this->datefin;
    }

    /**
     * @param string $datefin
     */
    public function setDatefin(string $datefin): void
    {
        $this->datefin = $datefin;
    }
}