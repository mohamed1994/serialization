<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 18/04/2019
 * Time: 14:28
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;


/**
 * Product
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="product", indexes={@ORM\Index(name="ville", columns={"ville_id"}),
 *     @ORM\Index(name="contact", columns={"contact_id"}),
 *
 * })
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="idProduct", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProduct;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;
    /**
     * @var string
     *
     * @ORM\Column(name="published", type="string")
     */
    private $published;
    /**
     * @var string
     *
     * @ORM\Column(name="updated", type="string")
     */
    private $updated;
    /**
     * @var string
     *
     * @ORM\Column(name="descriptifcommercialgeneral", type="string")
     */
    private $descriptifcommercialgeneral;
    /**
     * @var string
     *
     * @ORM\Column(name="descriptifgroupes", type="string")
     */
    private $descriptifgroupes;
    /**
     * @var string
     *
     * @ORM\Column(name="labelsmarquescertifications", type="string")
     */
    private $labelsmarquescertifications;
    /**
     * @var string
     *
     * @ORM\Column(name="planning", type="string")
     */
    private $planning;
    /**
     * @var string
     *
     * @ORM\Column(name="widgettripadvisor", type="string")
     */
    private $widgettripadvisor;
    /**
     * @var string
     *
     * @ORM\Column(name="tourismehandicap", type="string")
     */
    private $tourismehandicap;
    /**
     * @var string
     *
     * @ORM\Column(name="ObjectTypeName", type="string")
     */
    private $ObjectTypeName;
    /**
     * @var string
     *
     * @ORM\Column(name="ObjectTypeFix", type="string")
     */
    private $ObjectTypeFix;
    /**
     * @var string
     *
     * @ORM\Column(name="SyndicObjectID", type="string")
     */
    private $SyndicObjectID;
    /**
     * @var string
     *
     * @ORM\Column(name="SyndicObjectName", type="string")
     */
    private $SyndicObjectName;
    /**
     * @var string
     *
     * @ORM\Column(name="SyndicStructureId", type="string")
     */
    private $SyndicStructureId;
    /**
     * @var string
     *
     * @ORM\Column(name="SyndicObjectOrder", type="string")
     */
    private $SyndicObjectOrder;
    /**
     * @ORM\OneToOne(targetEntity="Villes", inversedBy="",cascade={"persist"})
     * @ORM\JoinColumn(name="ville_id", referencedColumnName="idVille")
     */
    private $ville;
    /**
     * @ORM\OneToOne(targetEntity="Contact", inversedBy="",cascade={"persist"})
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="idContact")
     */
    private $contact;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Modalites")
     * @ORM\JoinTable(name="prodmodalite",
     *   joinColumns={
     *     @ORM\JoinColumn(name="product_id", referencedColumnName="idProduct")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="modalite_id", referencedColumnName="idModalite")
     *   }
     * )
     */
    private $modalite;
    /**
     * @var string
     *
     * @ORM\Column(name="GmapLatitude", type="string")
     */
    private $GmapLatitude;
    /**
     * @var string
     *
     * @ORM\Column(name="GmapLongitude", type="string")
     */
    private $GmapLongitude;
    /**
     * @return string
     */
    public function getGmapLatitude(): string
    {
        return $this->GmapLatitude;
    }

    /**
     * @param string $GmapLatitude
     */
    public function setGmapLatitude(string $GmapLatitude): void
    {
        $this->GmapLatitude = $GmapLatitude;
    }

    /**
     * @return string
     */
    public function getGmapLongitude(): string
    {
        return $this->GmapLongitude;
    }

    /**
     * @param string $GmapLongitude
     */
    public function setGmapLongitude(string $GmapLongitude): void
    {
        $this->GmapLongitude = $GmapLongitude;
    }


    /**
     * @return int
     */
    public function getIdProduct(): int
    {
        return $this->idProduct;
    }

    /**
     * @param int $idProduct
     */
    public function setIdProduct(int $idProduct): void
    {
        $this->idProduct = $idProduct;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getPublished(): string
    {
        return $this->published;
    }

    /**
     * @param string $published
     */
    public function setPublished(string $published): void
    {
        $this->published = $published;
    }

    /**
     * @return string
     */
    public function getUpdated(): string
    {
        return $this->updated;
    }

    /**
     * @param string $updated
     */
    public function setUpdated(string $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getDescriptifcommercialgeneral(): string
    {
        return $this->descriptifcommercialgeneral;
    }

    /**
     * @param string $descriptifcommercialgeneral
     */
    public function setDescriptifcommercialgeneral(string $descriptifcommercialgeneral): void
    {
        $this->descriptifcommercialgeneral = $descriptifcommercialgeneral;
    }

    /**
     * @return string|null
     */
    public function getDescriptifgroupes(): string
    {
        return $this->descriptifgroupes;
    }

    /**
     * @param string $descriptifgroupes
     */
    public function setDescriptifgroupes(string $descriptifgroupes): void
    {
        $this->descriptifgroupes = $descriptifgroupes;
    }

    /**
     * @return string
     */
    public function getLabelsmarquescertifications(): string
    {
        return $this->labelsmarquescertifications;
    }

    /**
     * @param string $labelsmarquescertifications
     */
    public function setLabelsmarquescertifications(string $labelsmarquescertifications): void
    {
        $this->labelsmarquescertifications = $labelsmarquescertifications;
    }

    /**
     * @return string
     */
    public function getPlanning(): string
    {
        return $this->planning;
    }

    /**
     * @param string $planning
     */
    public function setPlanning(string $planning): void
    {
        $this->planning = $planning;
    }

    /**
     * @return string
     */
    public function getWidgettripadvisor(): string
    {
        return $this->widgettripadvisor;
    }

    /**
     * @param string $widgettripadvisor
     */
    public function setWidgettripadvisor(string $widgettripadvisor): void
    {
        $this->widgettripadvisor = $widgettripadvisor;
    }

    /**
     * @return string
     */
    public function getTourismehandicap(): string
    {
        return $this->tourismehandicap;
    }

    /**
     * @param string $tourismehandicap
     */
    public function setTourismehandicap(string $tourismehandicap): void
    {
        $this->tourismehandicap = $tourismehandicap;
    }

    /**
     * @return string
     */
    public function getObjectTypeName(): string
    {
        return $this->ObjectTypeName;
    }

    /**
     * @param string $ObjectTypeName
     */
    public function setObjectTypeName(string $ObjectTypeName): void
    {
        $this->ObjectTypeName = $ObjectTypeName;
    }

    /**
     * @return string
     */
    public function getObjectTypeFix(): string
    {
        return $this->ObjectTypeFix;
    }

    /**
     * @param string $ObjectTypeFix
     */
    public function setObjectTypeFix(string $ObjectTypeFix): void
    {
        $this->ObjectTypeFix = $ObjectTypeFix;
    }

    /**
     * @return string
     */
    public function getSyndicObjectID(): string
    {
        return $this->SyndicObjectID;
    }

    /**
     * @param string $SyndicObjectID
     */
    public function setSyndicObjectID(string $SyndicObjectID): void
    {
        $this->SyndicObjectID = $SyndicObjectID;
    }

    /**
     * @return string
     */
    public function getSyndicObjectName(): string
    {
        return $this->SyndicObjectName;
    }

    /**
     * @param string $SyndicObjectName
     */
    public function setSyndicObjectName(string $SyndicObjectName): void
    {
        $this->SyndicObjectName = $SyndicObjectName;
    }

    /**
     * @return string
     */
    public function getSyndicStructureId(): string
    {
        return $this->SyndicStructureId;
    }

    /**
     * @param string $SyndicStructureId
     */
    public function setSyndicStructureId(string $SyndicStructureId): void
    {
        $this->SyndicStructureId = $SyndicStructureId;
    }

    /**
     * @return string
     */
    public function getSyndicObjectOrder(): string
    {
        return $this->SyndicObjectOrder;
    }

    /**
     * @param string $SyndicObjectOrder
     */
    public function setSyndicObjectOrder(string $SyndicObjectOrder): void
    {
        $this->SyndicObjectOrder = $SyndicObjectOrder;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     */
    public function setVille($ville): void
    {
        $this->ville = $ville;
    }
    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact): void
    {
        $this->contact = $contact;
    }
    public function getModalite()
    {
        return $this->modalite;
    }


    public function setModalite($modalite): void
    {
        $this->modalite = $modalite;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->modalite = new \Doctrine\Common\Collections\ArrayCollection();
    }
}
