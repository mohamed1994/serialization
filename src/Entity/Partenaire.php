<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 27/03/2019
 * Time: 12:31
 */

namespace App\Entity;


class Partenaire
{
    private $id;
    private $intPartenaire;
    private $logoPartenaire;
    private $webPartenaire;
    private $emailPartenaire;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIntPartenaire()
    {
        return $this->intPartenaire;
    }

    /**
     * @param mixed $intPartenaire
     */
    public function setIntPartenaire($intPartenaire): void
    {
        $this->intPartenaire = $intPartenaire;
    }

    /**
     * @return mixed
     */
    public function getLogoPartenaire()
    {
        return $this->logoPartenaire;
    }

    /**
     * @param mixed $logoPartenaire
     */
    public function setLogoPartenaire($logoPartenaire): void
    {
        $this->logoPartenaire = $logoPartenaire;
    }

    /**
     * @return mixed
     */
    public function getWebPartenaire()
    {
        return $this->webPartenaire;
    }

    /**
     * @param mixed $webPartenaire
     */
    public function setWebPartenaire($webPartenaire): void
    {
        $this->webPartenaire = $webPartenaire;
    }

    /**
     * @return mixed
     */
    public function getEmailPartenaire()
    {
        return $this->emailPartenaire;
    }

    /**
     * @param mixed $emailPartenaire
     */
    public function setEmailPartenaire($emailPartenaire): void
    {
        $this->emailPartenaire = $emailPartenaire;
    }
}
