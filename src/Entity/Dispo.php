<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 27/03/2019
 * Time: 12:33
 */

namespace App\Entity;


class Dispo
{
    private $annee;

    /**
     * @return mixed
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * @param mixed $annee
     */
    public function setAnnee($annee): void
    {
        $this->annee = $annee;
    }
}
