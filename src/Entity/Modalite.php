<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 25/03/2019
 * Time: 09:19
 */

namespace App\Entity;


class Modalite
{
    private $id;
    private $num;
    private $modalite;
    private $logo;
    private $valModalite;
    private $logoModalite;
    private $intModalite;

    /**
     * @return mixed
     */
    public function getIntModalite()
    {
        return $this->intModalite;
    }

    /**
     * @param mixed $intModalite
     */
    public function setIntModalite($intModalite): void
    {
        $this->intModalite = $intModalite;
    }

    /**
     * @return mixed
     */
    public function getLogoModalite()
    {
        return $this->logoModalite;
    }

    /**
     * @param mixed $logoModalite
     */
    public function setLogoModalite($logoModalite): void
    {
        $this->logoModalite = $logoModalite;
    }

    /**
     * @return mixed
     */
    public function getValModalite()
    {
        return $this->valModalite;
    }

    /**
     * @param mixed $valModalite
     */
    public function setValModalite($valModalite): void
    {
        $this->valModalite = $valModalite;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * @param mixed $num
     */
    public function setNum($num): void
    {
        $this->num = $num;
    }

    /**
     * @return mixed
     */
    public function getModalite()
    {
        return $this->modalite;
    }

    /**
     * @param mixed $modalite
     */
    public function setModalite($modalite): void
    {
        $this->modalite = $modalite;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     */
    public function setLogo($logo): void
    {
        $this->logo = $logo;
    }
}
