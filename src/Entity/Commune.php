<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 27/03/2019
 * Time: 15:36
 */

namespace App\Entity;


class Commune
{
    private $codeInsee;
    private $commune;

    /**
     * @return mixed
     */
    public function getCodeInsee()
    {
        return $this->codeInsee;
    }

    /**
     * @param mixed $codeInsee
     */
    public function setCodeInsee($codeInsee): void
    {
        $this->codeInsee = $codeInsee;
    }

    /**
     * @return mixed
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * @param mixed $commune
     */
    public function setCommune($commune): void
    {
        $this->commune = $commune;
    }
}
