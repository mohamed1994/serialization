<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 23/04/2019
 * Time: 15:42
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contact
 *
 * @ORM\Table(name="contact"),
 *
 * })
 * @ORM\Entity
 */
class Contact
{
    /**
     * @var int
     *
     * @ORM\Column(name="idContact", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idContact;
    /**
     * @var string
     *
     * @ORM\Column(name="siteweb", type="string")
     */
    private $siteweb;
    /**
     * @var string
     *
     * @ORM\Column(name="adresse1", type="string")
     */
    private $adresse1;
    /**
     * @var string
     *
     * @ORM\Column(name="adresse2", type="string")
     */
    private $adresse2;
    /**
     * @var string
     *
     * @ORM\Column(name="adresse3", type="string")
     */
    private $adresse3;
    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string")
     */
    private $fax;
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string")
     */
    private $email;
    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string")
     */
    private $telephone;
    /**
     * @return int
     */
    public function getIdContact(): int
    {
        return $this->idContact;
    }

    /**
     * @param int $idContact
     */
    public function setIdContact(int $idContact): void
    {
        $this->idContact = $idContact;
    }

    /**
     * @return string
     */
    public function getSiteweb(): string
    {
        return $this->siteweb;
    }

    /**
     * @param string $siteweb
     */
    public function setSiteweb(string $siteweb): void
    {
        $this->siteweb = $siteweb;
    }

    /**
     * @return string
     */
    public function getAdresse1(): string
    {
        return $this->adresse1;
    }

    /**
     * @param string $adresse1
     */
    public function setAdresse1(string $adresse1): void
    {
        $this->adresse1 = $adresse1;
    }

    /**
     * @return string
     */
    public function getAdresse2(): string
    {
        return $this->adresse2;
    }

    /**
     * @param string $adresse2
     */
    public function setAdresse2(string $adresse2): void
    {
        $this->adresse2 = $adresse2;
    }

    /**
     * @return string
     */
    public function getAdresse3(): string
    {
        return $this->adresse3;
    }

    /**
     * @param string $adresse3
     */
    public function setAdresse3(string $adresse3): void
    {
        $this->adresse3 = $adresse3;
    }

    /**
     * @return string
     */
    public function getFax(): string
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax(string $fax): void
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone(string $telephone): void
    {
        $this->telephone = $telephone;
    }
}