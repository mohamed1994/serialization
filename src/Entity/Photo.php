<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 23/04/2019
 * Time: 16:13
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Photo
 *
 * @ORM\Table(name="photo",indexes={@ORM\Index(name="product", columns={"product_id"}),
 * })
 *
 * @ORM\Entity
 */
class Photo
{
    /**
     * @var int
     *
     * @ORM\Column(name="idPhoto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPhoto;
    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string")
     */
    private $photo;
    /**
     * @ORM\ManyToOne(targetEntity="Product",cascade={"persist"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="idProduct")
     */
    private $product;
    /**
     * @return int
     */
    public function getProduct()
    {
        return $this->product;
    }
    public function setProduct($product)
    {
        $this->product = $product;
    }
    /**
     * @return int
     */
    public function getIdPhoto(): int
    {
        return $this->idPhoto;
    }

    /**
     * @param int $idPhoto
     */
    public function setIdPhoto(int $idPhoto): void
    {
        $this->idPhoto = $idPhoto;
    }

    /**
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }
}
