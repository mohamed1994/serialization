<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 27/03/2019
 * Time: 14:17
 */

namespace App\Entity;


class Pays
{
    private $id;
    private $intPays;
    private $logoPays;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIntPays()
    {
        return $this->intPays;
    }

    /**
     * @param mixed $intPays
     */
    public function setIntPays($intPays): void
    {
        $this->intPays = $intPays;
    }

    /**
     * @return mixed
     */
    public function getLogoPays()
    {
        return $this->logoPays;
    }

    /**
     * @param mixed $logoPays
     */
    public function setLogoPays($logoPays): void
    {
        $this->logoPays = $logoPays;
    }
}
