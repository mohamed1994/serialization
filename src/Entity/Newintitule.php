<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 27/03/2019
 * Time: 12:30
 */

namespace App\Entity;


class Newintitule
{
    private $newIntitule ;
    private $lang;

    /**
     * @return mixed
     */
    public function getNewIntitule()
    {
        return $this->newIntitule;
    }

    /**
     * @param mixed $newIntitule
     */
    public function setNewIntitule($newIntitule): void
    {
        $this->newIntitule = $newIntitule;
    }

    /**
     * @return mixed
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param mixed $lang
     */
    public function setLang($lang): void
    {
        $this->lang = $lang;
    }
}
