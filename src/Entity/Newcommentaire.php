<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 29/03/2019
 * Time: 15:09
 */

namespace App\Entity;


class Newcommentaire
{
    private $newcommentaire;

    /**
     * @return mixed
     */
    public function getNewcommentaire()
    {
        return $this->newcommentaire;
    }

    /**
     * @param mixed $newcommentaire
     */
    public function setNewcommentaire($newcommentaire): void
    {
        $this->newcommentaire = $newcommentaire;
    }
}
