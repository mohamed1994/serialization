<?php
/**
 * Created by PhpStorm.
 * User: moham
 * Date: 25/03/2019
 * Time: 09:10
 */

namespace App\Entity;


class Critere
{
    private $id;
    private $idsToutesCategories;
    private $princ;
    private $type;
    private $intCritere;
    private $modalites;

    /**
     * @return mixed
     */
    public function getModalites()
    {
        return $this->modalites;
    }

    /**
     * @param mixed $modalites
     */
    public function setModalites($modalites): void
    {
        $this->modalites = $modalites;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdsToutesCategories()
    {
        return $this->idsToutesCategories;
    }

    /**
     * @param mixed $idsToutesCategories
     */
    public function setIdsToutesCategories($idsToutesCategories): void
    {
        $this->idsToutesCategories = $idsToutesCategories;
    }

    /**
     * @return mixed
     */
    public function getPrinc()
    {
        return $this->princ;
    }

    /**
     * @param mixed $princ
     */
    public function setPrinc($princ): void
    {
        $this->princ = $princ;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getIntCritere()
    {
        return $this->intCritere;
    }

    /**
     * @param mixed $intCritere
     */
    public function setIntCritere($intCritere): void
    {
        $this->intCritere = $intCritere;
    }
}
