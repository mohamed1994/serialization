<?php


namespace App;


interface AllFunctionServiceInterface
{
    public  function serializerFunction();
    public  function denormalizeModalities($tab);
    public  function denormalizeCategories($tab);
    public  function denormalizeVille($tab);
    public  function denormalizePays($tab);
    public  function denormalizePartenaire($tab);
    public  function denormalizeDatevalidite($tab);
    public  function denormalizeCommentaire($tab);
    public  function denormalizeAdresses($tab);
    public  function denormalizeCommune($tab);
    public  function denormalizePaysMonde($tab);
    public  function denormalizeCritere($tab);
    public  function denormalizeModalitiesCritere($tab);
    public  function denormalizeDispo($tab);
    public  function denormalizeNewPhoto($tab);
    public  function denormalizeDetailProduct($tab);
    public  function denormalizeDetailProductMap($tab);
    public  function denormalizeListCategories($tab);
    public  function denormalizeListProduct($tab);
    public  function denormalizeListCritere($tab);
    public  function domData($file);
    public  function denormalizeProduct($tab,$author);
    public  function denormalizeContact($tab);
    public  function denormalizePhoto($tab);
    public  function findYears($tab);
    public  function denormalizeVilles($tab);
    public  function deserializeAndSendDataToDataBase($src);
    public  function delimitedCharacter($data);
    public  function stringVerification($data);
    public  function searchCritere($data);
    public  function findCritere($data);
    public  function findDateDebut($data);
    public  function findDateFin($data);
}
