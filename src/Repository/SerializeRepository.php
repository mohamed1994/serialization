<?php

namespace App\Repository;

use Doctrine\ORM\EntityManagerInterface;

final class SerializeRepository
{
    private $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCritereBy($data)
    {
        $queryBuilder = $this->em->createQueryBuilder();
        return  $queryBuilder
            ->select('COUNT(c)')
            ->from('App\Entity\Criteres', 'c')
            ->where('c.intCritere = :data')
            ->setParameter('data', $data)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }
    public function searchProductBy($datedebut,$datefin,$id,$SyndicObjectName){
        $products = null;
        if ($SyndicObjectName !== ''){
            $queryBuilder = $this->em->createQueryBuilder();
            $products = $queryBuilder
                ->select('prod.nom')
                ->from('App\Entity\Periode','p')
                ->from('App\Entity\Product','prod')
                ->where('p.product = prod.idProduct')
                ->andWhere('prod.ObjectTypeName like :typehebergement')
                ->andWhere('p.datedebut = :datedebut')
                ->andWhere('p.datefin = :datefin')
                ->setParameter('typehebergement','%'.$SyndicObjectName.'%')
                ->setParameter('datedebut', $datedebut)
                ->setParameter('datefin', $datefin)
                ->getQuery()->getResult();
        }elseif($id !== ''){
            $queryBuilder = $this->em->createQueryBuilder();
            $products = $queryBuilder
                ->select('prod.nom')
                ->from('App\Entity\Periode','p')
                ->from('App\Entity\Product','prod')
                ->where('p.product = prod.idProduct')
                ->andWhere('p.datedebut = :datedebut')
                ->andWhere('p.datefin = :datefin')
                ->andWhere('prod.SyndicObjectID = :id')
                ->setParameter('datedebut', $datedebut)
                ->setParameter('datefin', $datefin)
                ->setParameter('id', $id)
                ->getQuery()->getResult();
        }
        return $products;
    }
}
