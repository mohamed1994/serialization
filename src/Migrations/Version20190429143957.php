<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190429143957 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE categorie (idCategorie INT AUTO_INCREMENT NOT NULL, intCategorie VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(idCategorie)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contact (idContact INT AUTO_INCREMENT NOT NULL, siteweb VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci, adresse1 VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, adresse2 VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci, adresse3 VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci, fax VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci, email VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci, telephone VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(idContact)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE critere (categorie_id INT DEFAULT NULL, idCritere INT AUTO_INCREMENT NOT NULL, intCritere VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, INDEX categorie_id (categorie_id), PRIMARY KEY(idCritere)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE modalite (critere_id INT DEFAULT NULL, idModalite INT AUTO_INCREMENT NOT NULL, intModalite VARCHAR(1000) DEFAULT NULL COLLATE utf8_general_ci, INDEX critere_id (critere_id), PRIMARY KEY(idModalite)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE periode (product_id INT DEFAULT NULL, idPeriode INT AUTO_INCREMENT NOT NULL, periode VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci, annee VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, INDEX product_id (product_id), PRIMARY KEY(idPeriode)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE photo (product_id INT DEFAULT NULL, idPhoto INT AUTO_INCREMENT NOT NULL, photo VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, INDEX product_id (product_id), PRIMARY KEY(idPhoto)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE prodmodalite (product_id INT NOT NULL, modalite_id INT NOT NULL, INDEX modalite_id (modalite_id), INDEX IDX_99E8281A4584665A (product_id), PRIMARY KEY(product_id, modalite_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product (categorie_id INT DEFAULT NULL, ville_id INT DEFAULT NULL, contact_id INT DEFAULT NULL, idProduct INT AUTO_INCREMENT NOT NULL, title VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, nom VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, author VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, published VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, updated VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, descriptifcommercialgeneral VARCHAR(1000) DEFAULT NULL COLLATE utf8_general_ci, descriptifgroupes VARCHAR(1000) DEFAULT NULL COLLATE utf8_general_ci, labelsmarquescertifications VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, ObjectTypeFix VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, ObjectTypeName VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, planning VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, tourismehandicap VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, widgettripadvisor VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, SyndicObjectID VARCHAR(100) NOT NULL COLLATE utf8_general_ci, SyndicObjectName VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, SyndicStructureId VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, SyndicObjectOrder VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, INDEX ville_id (ville_id), INDEX categorie_id (categorie_id), INDEX contact_id (contact_id), PRIMARY KEY(idProduct)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ville (idVille INT AUTO_INCREMENT NOT NULL, codepostal VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci, latitude VARCHAR(100) DEFAULT NULL COLLATE utf8_general_ci, longitude VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci, codeinsee VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(idVille)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE categorie');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contact');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE critere');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE modalite');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE periode');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE photo');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE prodmodalite');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE product');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE ville');
    }
}
