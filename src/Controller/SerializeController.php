<?php

namespace App\Controller;


use App\AllFunctionServiceInterface;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SerializeController extends AbstractController
{
    private $service;
    private $em;

    /**
     * SerializeController constructor.
     * @param AllFunctionServiceInterface $service
     * @param EntityManagerInterface $em
     */
    public function __construct(AllFunctionServiceInterface $service,EntityManagerInterface $em)
    {
        $this->service = $service;
        $this->em = $em;
    }
    /**
     * @return Serializer
     */
    public function serializerFunction()
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new GetSetMethodNormalizer(), new ArrayDenormalizer(), new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        return $serializer;
    }
    /**
     * @Route("/", name="index")
     */
    public function index(){
        $flux = $this->getParameter('flux');
        return $this->render('serialize/index.html.twig',['flux' => $flux]);
    }

    /**
     * @Route("/{link}/listCategorie", name="list_categories")
     * @param $link
     * @return Response
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function listCategories($link)
    {
        $cache = new FilesystemCache();
        $url = substr_replace( $this->getParameter('link').'Categories', $link, 8, 0 );
        $file = $this->service->domData($url);
        $lin = $cache->get('link');
        if($lin == $link) {
            $data = utf8_decode($cache->get('list.categories'));
        }else{
            $cache->setMultiple([
                'list.categories' => $file,
                'link' => $link,
            ]);
            $data = utf8_decode($cache->get('list.categories'));
        }
        $categories = $this->service->denormalizeListCategories($data);
        return $this->render('serialize/list_categories.html.twig', ['categories' => $categories,'link' => $link]);
    }

    /**
     * @Route("/{link}/listProduit/{idc}", name="list_product")
     * @param $idc
     * @param $link
     * @return Response
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function listProduct($idc,$link)
    {
        $cache = new FilesystemCache();
        $url = substr_replace( $this->getParameter('link').'Recherche&idC=', $link, 8, 0 );
        $data = $this->service->domData($url.$idc);
        $serializer = $this->serializerFunction();
        $lin = $cache->get( 'link');
        $product = $cache->get('list.product');
        if($lin == $link && $product == $data) {
            $data = $cache->get('list.product');
            $results = $serializer->decode(utf8_decode($data), 'xml');
        }else{
            $cache->setMultiple([
                'list.product' => $data,
                'link' => $link,
            ]);
            $data = $cache->get('list.product');
            $results = $serializer->decode(utf8_decode($data), 'xml');
        }
        $products = $this->service->denormalizeListProduct($results);
        return $this->render('serialize/list_product.html.twig', ['produits' => $products, 'nbrtotal' => $results['@nbTotal'],'link' => $link]);
    }

    /**
     * @Route("/{link}/listProduit/detailProduit/{idp}", name="detail_product")
     * @param $idp
     * @param $link
     * @return Response
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function detailProduct($idp,$link){
        $serializer = $this->serializerFunction();
        $cache = new FilesystemCache();
        $url = substr_replace( $this->getParameter('link').'Produit&idP=', $link, 8, 0 );
        $data = $this->service->domData($url . $idp);
        $lin = $cache->get( 'link');
        $detail_product = $cache->get('list.detail.product');
        if($lin == $link && $detail_product == $data) {
            $details = $this->service->denormalizeDetailProduct($detail_product);
        }else{
            $cache->setMultiple([
                'list.detail.product' => $data,
                'link' => $link,
            ]);
            $data = $cache->get('list.detail.product');
            $details = $this->service->denormalizeDetailProduct($data);
        }
        $coordonne = $serializer->serialize($details,'json',['skip_null_values' => true]);
        return $this->render('serialize/detail_product.html.twig',['details' => $details,'coordonne'=>$coordonne,'link' => $link]);
    }


    /**
     * @Route("/{link}/listCritere/{idc}", name="list_critere")
     * @param $idc
     * @param $link
     * @return Response
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function listCriteres($idc,$link)
    {
        $cache = new FilesystemCache();
        $serializer = $this->serializerFunction();
        $url = substr_replace( $this->getParameter('link').'Criteres&idC=', $link, 8, 0 );
        $file = utf8_decode($this->service->domData($url . $idc));
        $data = $serializer->decode($file, 'xml');
        $lin = $cache->get( 'link');
        $list = $cache->get('list.critere');
        if($lin == $link && $list == $data) {
            $criteres = $this->service->denormalizeListCritere($list);
        }else{
            $cache->setMultiple([
                'list.critere' => $data,
                'link' => $link,
            ]);
            $criteres = $this->service->denormalizeListCritere($cache->get('list.critere'));
        }
        return $this->render('serialize/list_critere.html.twig', ['criteres' => $criteres, 'link' => $link,'categorie' => $data['categorie']['#']]);
    }

    /**
     * @Route("/{link}/listProduitinMap/{idc}", name="list_product_in_map")
     * @param $idc
     * @param $link
     * @return Response
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function listProductMap($idc,$link){
        $details = [];
        $cache = new FilesystemCache();
        $serializer = $this->serializerFunction();
        $url_detail = substr_replace( $this->getParameter('link').'Produit&idP=', $link, 8, 0 );
        $url_list = substr_replace( $this->getParameter('link').'Recherche&idC=', $link, 8, 0 );
        $data = $this->service->domData($url_list . $idc);
        $lin = $cache->get( 'link');
        $list = $cache->get('list.product');
        if($lin == $link && $list == $data) {
            $results = $serializer->decode($cache->get('list.product'), 'xml');
        }else{
            $cache->setMultiple([
                'list.product' => utf8_decode($data),
                'link' => $link,
            ]);
            $results = $serializer->decode($cache->get('list.product'), 'xml');
        }
        if ($results['@nbTotal'] == 1) {
            $data = $this->service->domData($url_detail . $results['produit']['@id']);
            $details[] = $this->service->denormalizeDetailProductMap(utf8_decode($data));
        } elseif ($results['@nbTotal'] == 0) {
            $details = $serializer->denormalize(['id' => ''], 'App\Entity\Produit', 'xml');
        } else {
            foreach ($results['produit'] as $row) {
                $data = $this->service->domData($url_detail . $row['@id']);
                $details[] = $this->service->denormalizeDetailProductMap(utf8_decode($data));
            }
        }
        $result = $serializer->serialize($details,'json',['skip_null_values' => true]);
        return $this->render('serialize/list_product_in_map.html.twig',['result' => $result,'link' => $link]);
    }

    /**
     * @Route("/product", name="product")
     * @return Response
     */
    public function serializeProduct(){
        $serializer = $this->serializerFunction();
        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();
        $product = $serializer->serialize($product,'xml');
        $response = new Response($product);
        $response->headers->set('Content-Type', 'text/xml');
        return new Response($response);
    }

    /**
     * @Route("/search", name="search")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response
     */
    public function searchProduct(Request $request)
    {
        $serializer = $this->serializerFunction();
        $id = $request->get('id');
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();
        if($id == ''){
            return $this->render('serialize/search.html.twig',['products' => $products]);
        }else{
            $file = $this->service->domData(substr_replace( $this->getParameter('link').'Recherche&ref=', 'dev-oise-et-halatte-dw', 8, 0 ).$id);
            $products = $serializer->decode($file, 'xml');
            $products = $serializer->decode(utf8_decode($this->service->domData(substr_replace( $this->getParameter('link').'Produit&idP=', 'dev-oise-et-halatte-dw', 8, 0 ).$products['produit']['@id'])), 'xml');
            return new JsonResponse(['products' => $products]);
        }
    }
}
