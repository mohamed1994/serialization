<?php
namespace App\Commands;

use App\AllFunctionServiceInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Style\SymfonyStyle;


class SerializeCommand extends Command
{
    private $service;

    /**
     * SerializeController constructor.
     * @param AllFunctionServiceInterface $service
     */
    public function __construct(AllFunctionServiceInterface $service)
    {
        parent::__construct();
        $this->service = $service;
    }
    protected function configure()
    {
        $this->setName('make:product')
            ->setDescription('serialization object')
            ->setHelp('Demonstration of custom commands created by Symfony Console component.')
            ->addArgument('object', InputArgument::REQUIRED, 'Add resource');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $i = 0;
        $io = new SymfonyStyle($input, $output);
        $txt = "you find the xml file in this link";
        if($io->confirm('added data in database?', true)){
            if (preg_match('#^http://wcf.tourinsoft.com/Syndication/oise/c1d98da1-265d-42ec-8a83-800b57f855d7/Objects#', $input->getArgument('object'))) {
                $data = $this->service->deserializeAndSendDataToDataBase($input->getArgument('object'));
                $progressbar = new ProgressBar($output, 1);
                while ($i++ < 1) {
                    $progressbar->advance();
                }
                $progressbar->finish();
                $io->success([$data,$txt.' http://serialization.local/product']);
            }else{
                $io->error('Source not found');
            }
        }
    }
}
