(function ($) {
    "use strict";

})(jQuery);
$(document).ready(function() {
    $('#example').DataTable( {
        columnDefs: [
            {
                targets: [ 0, 1, 2 ],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    } );
} );

$(document).ready(function() {
    $('.flexslider').flexslider({
        animation: "slide"
    });
});

$('.title-critere').on('click',function(){
    $(this).toggleClass('critere-active');
});
/////////////////////////////////////////
$('.entry-header').on('click',function(){
    $(this).toggleClass('modalite-active');
});
/*--Scroll Back to Top Button Show--*/
$(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
        $('#back-to-top').addClass('icon-top')
    } else {
        $('#back-to-top').removeClass('icon-top');
    }
});

//Click event scroll to top button jquery

$('#back-to-top').click(function(){
    $('html, body').animate({scrollTop : 0},600);
    return false;
});
//////////////////////////////////////
$('.icon').on('click',function(){
    $('.site-navigation').toggleClass('active').fadeOut( 1500, "linear");
});
//////////////////////////////////////

function searchCritere() {
    let input, filter, ul, li, p, i, txtValue;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    ul = document.getElementById("critere");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        p = li[i].getElementsByTagName("p")[0];
        txtValue = p.textContent || p.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
//////////////////////////////////////
